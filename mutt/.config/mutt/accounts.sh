#!/bin/sh

for account in $HOME/.config/mutt/accounts/*.muttrc
do
    address=$(basename $account .muttrc)
    echo "source $account"
    echo "folder-hook $address source $account"
done
