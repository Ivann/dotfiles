#!/bin/sh

pkill polybar

while grep -u $UID -x polybar >/dev/null; do sleep 1; done

xrandr_output=$(xrandr)
connected=$(echo "$xrandr_output" | awk '/ connected/{print $1}')
primary=$(echo "$xrandr_output" | awk '/ primary/{print $1}')

hostname=$(hostname)

network_interfaces=$(ip route | grep '^default' | awk '{print $5}')
wired_network_interface=$(echo "$network_interfaces" | grep '^enp' | head -n1)
wireless_network_interface=$(echo "$network_interfaces" | grep '^wlp' | head -n1)

for c in $connected
do
    if [ "$c" = "$primary" ]
    then
        MONITOR=$c \
        WIRED_INTERFACE=$wired_network_interface \
        WIRELESS_INTERFACE=$wireless_network_interface \
        polybar "$hostname" &
    else
        MONITOR=$c polybar secondary &
    fi
done
