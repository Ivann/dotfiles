ZSH_CACHE_DIR=$HOME/.cache/zsh
[ -d "$ZSH_CACHE_DIR" ] || mkdir -p "$ZSH_CACHE_DIR"

### Modules {{{

autoload -U compinit run-help run-help-git edit-command-line history-search-end colors
colors

### }}}

### Options {{{

## INPUT/OUTPUT

# spell check commands
setopt CORRECT

# reduce keytimeout
export KEYTIMEOUT=1

## CHANGING DIRECTORIES

# cd = pushd
setopt AUTO_PUSHD

# use +x or -x to go to a dir in the stack
setopt PUSHD_MINUS

# make pushd silent
setopt PUSHD_SILENT

# blank pushd goes to home
setopt PUSHD_TO_HOME

# remove duplicate
setopt PUSHD_IGNORE_DUPS

## JOB CONTROL

# report background and suspended jobs before exiting a shell
setopt CHECK_JOBS

## COMPLETION

setopt ALWAYS_LAST_PROMPT

setopt AUTO_MENU

setopt COMPLETE_IN_WORD

# expand glob
setopt GLOB_COMPLETE

# don't throw an error when no match are found
unsetopt NOMATCH

# hash the entire command
setopt HASH_LIST_ALL

## ZLE

# disable beeps
setopt NO_BEEP

## EXPANSION AND GLOBBING

# sort numeric filenames numerically
setopt NUMERIC_GLOB_SORT

# allow #, ~ and ^ in filename patterns
setopt EXTENDED_GLOB

# better array expansion
setopt RC_EXPAND_PARAM

## HISTORY

# write to the history file immediately, not when the shell exits
setopt INC_APPEND_HISTORY

# share history
setopt SHARE_HISTORY

# Expire duplicate entries first when trimming history
setopt HIST_EXPIRE_DUPS_FIRST

# ignore duplicate
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_SAVE_NO_DUPS

# do not save command starting by a space to the history
setopt HIST_IGNORE_SPACE

# Remove superfluous blanks before recording entry
setopt HIST_REDUCE_BLANKS

# Don't execute immediately upon history expansion
setopt HIST_VERIFY

# do not autoupdate
DISABLE_AUTO_UPDATE=true

### }}}

### Completion {{{

zstyle ':completion:*' accept-exact '*(N)'
# completion caching, rehash to clear
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$ZSH_CACHE_DIR/completion"

# menu if nb items > 2
zstyle ':completion:*' menu select=2

# hilight ambiguous character
zstyle ':completion:*' show-ambiguity

# colors
zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)*==34=34}:${(s.:.)LS_COLORS}")';

# completers
zstyle ':completion:*' completer _complete _match _approximate

# Increase the number of errors based on the length of the typed word. But make
# sure to cap (at 7) the max-errors to avoid hanging.
zstyle ':completion:*:approximate:*' max-errors 1 numeric
zstyle -e ':completion:*:approximate:*' max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3>7?7:($#PREFIX+$#SUFFIX)/3))numeric)'

# ignore what's already on the line
zstyle ':completion:*:(rm|kill|killall|diff|cp|mv):*' ignore-line yes

zstyle ':completion:*' ignore-parents parents pwd

zstyle ':completion:*' list-separator '┆ '

zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%F{yellow}%B── %d ──%b%f'
zstyle ':completion:*:messages' format '%F{red}%d%f'
zstyle ':completion:*' group-name ''
zstyle ':completion:*:manuals' separate-sections true

zstyle ':completion:*:processes' command 'ps -u $LOGNAME -o pid,user,command -w'
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=29=34"

# Don't complete uninteresting users...
zstyle ':completion:*:*:*:users' ignored-patterns \
  adm amanda apache avahi beaglidx bin cacti canna clamav daemon \
  dbus distcache dovecot fax ftp games gdm gkrellmd gopher \
  hacluster haldaemon halt hsqldb ident junkbust ldap lp mail \
  mailman mailnull mldonkey mysql nagios \
  named netdump news nfsnobody nobody nscd ntp nut nx openvpn \
  operator pcap postfix postgres privoxy pulse pvm quagga radvd \
  rpc rpcuser rpm shutdown squid sshd sync uucp vcsa xfs '_*'

# ... unless we really want to.
zstyle '*' single-ignored show

zstyle :compinstall filename "$HOME/.config/zsh/.zshrc"

### }}}

### History {{{

HISTFILE="$ZSH_CACHE_DIR/histfile"
HISTSIZE=1000000000
SAVEHIST=$HISTSIZE

### }}}

### Bindkeys {{{

zle -N edit-command-line
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

bindkey -v

## bind special keys
# https://wiki.archlinux.org/index.php/Zsh#Key_bindings

# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -A key

key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}
key[STab]=${terminfo[kcbt]}

# setup key accordingly
[[ -n "${key[Home]}"     ]]  && bindkey  "${key[Home]}"     beginning-of-line
[[ -n "${key[End]}"      ]]  && bindkey  "${key[End]}"      end-of-line
[[ -n "${key[Insert]}"   ]]  && bindkey  "${key[Insert]}"   overwrite-mode
[[ -n "${key[Delete]}"   ]]  && bindkey  "${key[Delete]}"   delete-char
[[ -n "${key[Up]}"       ]]  && bindkey  "${key[Up]}"       history-beginning-search-backward-end
[[ -n "${key[Down]}"     ]]  && bindkey  "${key[Down]}"     history-beginning-search-forward-end
[[ -n "${key[Left]}"     ]]  && bindkey  "${key[Left]}"     backward-char
[[ -n "${key[Right]}"    ]]  && bindkey  "${key[Right]}"    forward-char
[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"   beginning-of-buffer-or-history
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}" end-of-buffer-or-history
[[ -n "${key[STab]}"     ]]  && bindkey  "${key[STab]}"     reverse-menu-complete


bindkey ' ' magic-space

bindkey -M viins '^p' history-beginning-search-backward-end
bindkey -M vicmd '^p' history-beginning-search-backward-end

bindkey -M viins '^n' history-beginning-search-forward-end
bindkey -M vicmd '^n' history-beginning-search-forward-end

bindkey -M viins '^w' backward-kill-word

# edit current command line in vim with CTRL-f
bindkey -M viins '^f'   edit-command-line
bindkey -M vicmd '^f'   edit-command-line

# open help with CTRL-h
bindkey -M viins '^h'   run-help
bindkey -M vicmd '^h'   run-help

bindkey "\e[A" history-beginning-search-backward-end
bindkey "\e[B" history-beginning-search-forward-end

### }}}

### Aliases {{{

alias hs='history | grep'
alias psg='ps aux | grep'
alias l='exa -l --git'
alias lt='exa -l --git --tree'
alias grep='grep --color'

if ls --color -d . >/dev/null 2>&1
then
    alias ls='ls --color'
else
    alias ls='ls -G'
fi

### }}}

source "$ZDOTDIR/plugins.zsh"

compinit -d "$ZSH_CACHE_DIR/zcompdump"

for f in $ZDOTDIR/conf.d/*(N); do
    source "$f"
done
