import os

# pylint: disable=C0111
c = c  # noqa: F821 pylint: disable=E0602,C0103
config = config  # noqa: F821 pylint: disable=E0602,C0103

c.auto_save.session = True
c.session.lazy_restore = True

c.tabs.show = 'switching'
c.tabs.show_switching_delay = 1000
c.tabs.position = 'left'
c.tabs.background = True

c.content.default_encoding = "utf-8"
c.content.headers.user_agent = ("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "
                                "(KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36")
c.content.webrtc_ip_handling_policy = 'default-public-interface-only'
c.content.canvas_reading = False
c.content.javascript.can_open_tabs_automatically = True

c.downloads.position = "bottom"
c.downloads.remove_finished = 0

c.editor.command = ["kitty", "--class", "floating", "-e", "nvim", "{}"]

c.hints.chars = "auiectsrnbpovdljmhfgqkxy"

c.spellcheck.languages = ["en-GB", "fr-FR"]

c.aliases = {'o': 'open'}

c.url.searchengines = {
    'DEFAULT': 'https://duckduckgo.com/?q={}',
    'aw': 'https://wiki.archlinux.org/?search={}',
    'g': 'https://www.google.com/search?hl=en&q={}'
}

config.source('themes/base16-onedark.config.py')

MY_BINDINGS = {
    'normal': {
        'c': 'scroll left',
        'r': 'scroll right',
        't': 'scroll down',
        's': 'scroll up',
        'C': 'back',
        'R': 'forward',
        'T': 'tab-next',
        'S': 'tab-prev',
        'gt': 'tab-move +',
        'gs': 'tab-move -',
        'gn': 'set-cmd-text -s :buffer',
        'èc': 'back --tab',
        'èr': 'forward --tab',
        'È': 'config-cycle tabs.show always switching',
        'hd': 'download-clear',
        'ho': 'tab-only',
        'J': 'tab-focus',
        ',b': 'open qute://bookmarks#bookmarks',
        ',h': 'open qute://history',
        ',q': 'open qute://bookmarks',
        ',s': 'open qute://settings',
        'f': 'fullscreen',
        'e': 'hint',
        'E': 'hint all tab',
        'we': 'hint all window',
        'l': 'reload',
        'L': 'reload --force',
        'F': 'yank selection --sel;; later 10 open --tab --related {primary}',
        ',l': 'config-cycle spellcheck.languages ["en-GB"] ["fr-FR"] ["en-GB", "fr-FR"]',
        ',a': ('spawn --userscript qute-pass --username-pattern "login: (.+)" '
               '--username-target "secret"'),
        ',u': ('spawn --userscript qute-pass --username-pattern "login: (.+)" '
               '--username-target "secret" --username-only'),
        ',p': ('spawn --userscript qute-pass --username-pattern "login: (.+)" '
               '--username-target "secret" --password-only'),
    },
    'caret': {
        'C': 'scroll left',
        'R': 'scroll right',
        'T': 'scroll down',
        'S': 'scroll up',
        'c': 'move-to-prev-char',
        'r': 'move-to-next-char',
        't': 'move-to-next-line',
        's': 'move-to-prev-line',
        'F': 'yank selection --sel;; later 10 open --tab --related {primary}',
    }
}

for mode, bindings in MY_BINDINGS.items():
    for key, cmd in bindings.items():
        config.bind(key, cmd, mode=mode)

if os.path.isfile(config.configdir / 'config.local.py'):
    config.source('config.local.py')
