nnoremap <leader>u :UndotreeToggle<cr>

let g:undotree_WindowLayout = 4

function g:Undotree_CustomMap()
    nmap <buffer> S <plug>UndotreeNextState
    nmap <buffer> T <plug>UndotreePreviousState
endfunc
