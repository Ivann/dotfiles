call denite#custom#option('_', {
    \ 'auto_accel': 1,
    \ 'vertical_preview': 1
    \ })

call denite#custom#source(
    \ 'file/rec/git',
    \ 'converters', ['devicons_denite_converter']
    \)

call denite#custom#alias('source', 'file/rec/git', 'file/rec')
call denite#custom#var('file/rec/git', 'command', ['git', 'ls-files', '-co', '--exclude-standard'])

" Ripgrep command on grep source
if executable('rg')
    call denite#custom#var('grep', 'command', ['rg'])
    call denite#custom#var('grep', 'default_opts', ['--hidden', '--vimgrep', '--no-heading', '-S'])
    call denite#custom#var('grep', 'recursive_opts', [])
    call denite#custom#var('grep', 'pattern_opt', ['--regexp'])
    call denite#custom#var('grep', 'separator', ['--'])
    call denite#custom#var('grep', 'final_opts', [])
endif

let s:insert_mode_mappings = [
    \ ['<C-t>', '<denite:move_to_next_line>'],
    \ ['<C-s>', '<denite:move_to_previous_line>'],
    \ ['<Esc>', '<denite:enter_mode:normal>'],
    \ ['<C-q>', '<denite:quit>']
    \]

let s:normal_mode_mappings = [
    \ ['t', '<denite:move_to_next_line>'],
    \ ['s', '<denite:move_to_previous_line>'],
    \ ['c', '<denite:move_caret_to_left>'],
    \ ['r', '<denite:move_caret_to_right>'],
    \ ['gg', '<denite:move_to_first_line>'],
    \ ['q', '<denite:quit>'],
    \ ['a', '<denite:do_action:add>'],
    \ ['d', '<denite:do_action:delete>'],
    \ ['h', '<denite:do_action:split>'],
    \ ['v', '<denite:do_action:vsplit>']
    \]

for m in s:insert_mode_mappings
    call denite#custom#map('insert', m[0], m[1], 'noremap')
endfor

for m in s:normal_mode_mappings
    call denite#custom#map('normal', m[0], m[1], 'noremap')
endfor

autocmd VimEnter * call s:denite_my_mappings()
function! s:denite_my_mappings() abort
    nnoremap <silent><leader>b :<C-u>Denite buffer<CR>
    nnoremap <silent><leader>p :<C-u>DeniteProjectDir file/rec/git<CR>
    nnoremap <silent><leader>o :<C-u>DeniteProjectDir file/rec<CR>
    nnoremap <silent><leader>a :<C-u>DeniteProjectDir grep:::!<CR>
    nnoremap <silent><leader>r :<C-u>Denite file_mru<CR>
    nnoremap <silent><leader>d :<C-u>Denite directory_rec<CR>
    nnoremap <silent><leader>k :<C-u>Denite directory_rec:~<CR>
    nnoremap <silent><leader>s :<C-u>Denite session<CR>

    nnoremap <silent><leader>gl :<C-u>Denite gitlog<CR>
    nnoremap <silent><leader>ga :<C-u>Denite gitlog:all<CR>
    nnoremap <silent><leader>gs :<C-u>Denite gitstatus<CR>
    nnoremap <silent><leader>gc :<C-u>Denite gitchanged<CR>
    nnoremap <silent><leader>gb :<C-u>Denite gitbranch<CR>
endfunction
