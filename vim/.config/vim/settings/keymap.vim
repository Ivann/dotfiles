" deactivate arrow keys
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
nnoremap <PageDown> <nop>
nnoremap <PageUp> <nop>
nnoremap <home> <nop>
nnoremap <end> <nop>

inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
inoremap <PageDown> <nop>
inoremap <PageUp> <nop>
inoremap <home> <nop>
inoremap <end> <nop>

vnoremap <up> <nop>
vnoremap <down> <nop>
vnoremap <left> <nop>
vnoremap <right> <nop>
vnoremap <PageDown> <nop>
vnoremap <PageUp> <nop>
vnoremap <home> <nop>
vnoremap <end> <nop>
