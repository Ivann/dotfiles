let g:abolish_no_mappings = 1

autocmd VimEnter * call s:abolish_my_mappings()
function! s:abolish_my_mappings() abort
    nmap <leader>c <Plug>Coerce
endfunction
