autocmd VimEnter * call s:defx_my_mappings()
function! s:defx_my_mappings() abort
    nmap <leader>f :Defx `expand('%:p:h')` -search=`expand('%:p')`<CR>
endfunction

autocmd FileType defx call s:defx_my_settings()
function! s:defx_my_settings() abort
    " Define mappings
    nnoremap <silent><buffer><expr> <CR> defx#do_action('open')
    nnoremap <silent><buffer><expr> r defx#do_action('open')
    nnoremap <silent><buffer><expr> c defx#do_action('cd', ['..'])
    nnoremap <silent><buffer><expr> h defx#do_action('multi', [['open', 'split'], 'quit'])
    nnoremap <silent><buffer><expr> v defx#do_action('multi', [['open', 'vsplit'], 'quit'])
    nnoremap <silent><buffer><expr> p defx#do_action('open', 'pedit')
    nnoremap <silent><buffer><expr> . defx#do_action('toggle_ignored_files')
    nnoremap <silent><buffer><expr> ~ defx#do_action('cd')
    nnoremap <silent><buffer><expr> q defx#do_action('quit')
    nnoremap <silent><buffer><expr> <C-n> defx#do_action('new_file')
    nnoremap <silent><buffer><expr> <C-c> defx#do_action('copy')
    nnoremap <silent><buffer><expr> <C-m> defx#do_action('move')
    nnoremap <silent><buffer><expr> <C-p> defx#do_action('paste')
    nnoremap <silent><buffer><expr> <C-d> defx#do_action('remove')
    nnoremap <silent><buffer><expr> <C-r> defx#do_action('rename')
endfunction
