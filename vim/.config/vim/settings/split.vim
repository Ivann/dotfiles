" move
nnoremap ét <C-W><C-J>
nnoremap és <C-W><C-K>
nnoremap ér <C-W><C-L>
nnoremap éc <C-W><C-H>

" resize
nnoremap <left> :vertical resize -5<cr>
nnoremap <down> :resize +5<cr>
nnoremap <up> :resize -5<cr>
nnoremap <right> :vertical resize +5<cr>

set splitbelow
set splitright
