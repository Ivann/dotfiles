autocmd VimEnter * call s:surround_my_mappings()
function! s:surround_my_mappings() abort
    if exists("g:loaded_surround") && (!exists("g:surround_no_mappings") || ! g:surround_no_mappings) && maparg('cs', 'n') !=# ''
        execute 'nmap ls' maparg('cs', 'n')
        execute 'nmap lS' maparg('cS', 'n')
        nunmap cs
        nunmap cS
    endif
endfunction
