let g:startify_session_dir = '~/.cache/vim/sessions'
let g:startify_session_persistence = 1
let g:startify_fortune_use_unicode = 1

let g:startify_lists = [
    \ { 'type': 'sessions',     'header': ['    Sessions']        },
    \ { 'type': 'bookmarks',    'header': ['    Bookmarks']       },
    \ { 'type': 'files',        'header': ['    MRU ']            },
    \ { 'type': 'dir',          'header': ['    MRU ' . getcwd()] },
    \ ]
