# Keybinding file for the vimiv image viewer
# Modifiers: Shift (through Shift+), Ctrl (through ^), Alt (through Alt+)
# Please refer to vimivrc(5) for further information

[IMAGE] ########################################################################
a:          autorotate
Shift+w:    center
Escape:     clear_status
colon:      command
Shift+y:    copy_abspath
y:          copy_basename
x:          delete
g:          first
e:          fit_horiz
Shift+e:    fit_vert
underscore: flip 0
bar:        flip 1
Shift+o:    focus_library
f:          fullscreen
Shift+g:    last
o:          library
h:          manipulate
m:          mark
Shift+m:    mark_toggle
u:          move_up
n:          next
p:          prev
^q:         q
q:          q
less:       rotate 1
greater:    rotate 3
c:          scroll h
Left:       scroll h
Shift+c:    scroll H
t:          scroll j
Down:       scroll j
Shift+t:    scroll J
s:          scroll k
Up:         scroll k
Shift+s:    scroll K
r:          scroll l
Right:      scroll l
Shift+r:    scroll L
slash:      search
Shift+n:    search_next
Shift+p:    search_prev
space:      set play_animations!
l:          set rescale_svg!
b:          set display_bar!
k:          slideshow
comma:      set slideshow_delay -0.2
period:     set slideshow_delay +0.2
j:          thumbnail
plus:       zoom_in
minus:      zoom_out
w:          zoom_to

Button2:    library
Button1:    next
Button3:    prev

[THUMBNAIL] ####################################################################
a:          autorotate
Escape:     clear_status
colon:      command
Shift+y:    copy_abspath
y:          copy_basename
x:          delete
g:          first
underscore: flip 0
bar:        flip 1
Shift+o:    focus_library
f:          fullscreen
Shift+g:    last
o:          library
m:          mark
Shift+m:    mark_toggle
u:          move_up
^q:         q
q:          q
less:       rotate 1
greater:    rotate 3
h:          scroll h
Left:       scroll h
Shift+c:    scroll H
t:          scroll j
Down:       scroll j
Shift+t:    scroll J
s:          scroll k
Up:         scroll k
Shift+s:    scroll K
r:          scroll l
Right:      scroll l
Shift+r:    scroll L
slash:      search
Shift+n:    search_next
Shift+p:    search_prev
l:          set rescale_svg!
b:          set display_bar!
comma:      set slideshow_delay -0.2
period:     set slideshow_delay +0.2
j:          thumbnail
plus:       zoom_in
minus:      zoom_out

[LIBRARY] ######################################################################
a:          autorotate
Shift+w:    center
Escape:     clear_status
colon:      command
Shift+y:    copy_abspath
y:          copy_basename
x:          delete
g:          first_lib
e:          fit_horiz
Shift+e:    fit_vert
underscore: flip 0
bar:        flip 1
f:          fullscreen
Shift+l:    set library_width +20
Shift+g:    last_lib
o:          library
m:          mark
Shift+m:    mark_toggle
u:          move_up
n:          next
p:          prev
^q:         q
q:          q
less:       rotate 1
greater:    rotate 3
c:          scroll_lib h
Left:       scroll_lib h
t:          scroll_lib j
Down:       scroll_lib j
s:          scroll_lib k
Up:         scroll_lib k
r:          scroll_lib l
Right:      scroll_lib l
slash:      search
Shift+n:    search_next
Shift+p:    search_prev
l:          set rescale_svg!
^h:         set show_hidden!
b:          set display_bar!
Shift+h:    set library_width -20
comma:      set slideshow_delay -0.2
period:     set slideshow_delay +0.2
j:          thumbnail
Shift+o:    unfocus_library
plus:       zoom_in
minus:      zoom_out
w:          zoom_to

Button3:    move_up

[MANIPULATE] ###################################################################
Space:      accept_changes
Return:     accept_changes
a:          autorotate
Shift+w:    center
colon:      command
Shift+y:    copy_abspath
y:          copy_basename
Escape:     discard_changes
w:          zoom_to
e:          fit_horiz
Shift+e:    fit_vert
underscore: flip 0
bar:        flip 1
b:          focus_slider bri
h:          focus_slider con
k:          focus_slider sat
f:          fullscreen
m:          mark
Shift+m:    mark_toggle
less:       rotate 1
greater:    rotate 3
c:          slider -1
r:          slider +1
Shift+c:    slider -10
Shift+r:    slider +10

[COMMAND] ######################################################################
Tab:        complete
Shift+Tab:  complete_inverse
Escape:     discard_command
^n:         history_down
Down:       history_down
^p:         history_up
Up:         history_up
^q:         q

# vim:ft=dosini
